//*********************************************
// Honor Code: The work I am submitting is a result of my own thinking and efforts.
// Kierra Price and Elizabeth Kroemer
// CMPSC 111 Spring 2017
// Lab 6
// Date: 02 23 2017
//
// Purpose: To have the user import a DNA string, and have the program mutate it in certain ways.
//*********************************************
import java.util.Date; // needed for printing today's date
import java.util.Random; // needed for generating Random numbers
import java.util.Scanner; // used for user import

public class dna
{
	public static void main(String[] args)
	{
		// Variable dictionary:
		Scanner scan = new Scanner(System.in);	// used for user import
		Random r = new Random();		// used for random numbers
		String dnaString, s2, s3, s4, s5;	// dnaString is user import, others are for mutations	
		int len;			// length of string
		int location;			// one of the positions in a string
		char c;				// a letter randomly chosen from string

		System.out.println("Kierra Price and Liz Kroemer\nLab 6\n" + new Date() + "\n---------------\n");
	
		// DNA String Imput
		System.out.println("Please enter a DNA String with only C, G, T, and A: ");
		dnaString = scan.next();
		dnaString = dnaString.toUpperCase();
		System.out.println("Your DNA String is: " + dnaString);

		// Compliment of DNA String
		s2 = dnaString.replace('A','T');
		s2 = dnaString.replace;
		

		System.out.println("Your compliment DNA is: " + s2);
		


	}
}
