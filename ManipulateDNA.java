//*********************************************
// Honor Code: The work I am submitting is a result of my own thinking and efforts.
// Kierra Price and Elizabeth Kroemer
// CMPSC 111 Spring 2017
// Lab 6
// Date: 02 23 2017
//
// Purpose: To have the user import a DNA string, and have the program mutate it in certain ways.
//*********************************************
import java.util.Date; // needed for printing today's date
import java.util.Random; // needed for generating Random numbers
import java.util.Scanner; // used for user import

public class ManipulateDNA
{
	public static void main(String[] args)
	{
		// Variable dictionary:
		Scanner scan = new Scanner(System.in);			// used for user import
		Random r = new Random();				// used for random numbers
		String dnaString, s2, s3, s4, s5, s6, s7, s8, s9, s10;	// dnaString is user import, others are for mutations 	
		int len;						// length of string
		int location, location2, location3;			// one of the positions in a string
		char a = "ACTG".charAt(r.nextInt(4));		// String of acceptable letters to choose randomly from
		char b = "ACTG".charAt(r.nextInt(4));
	
		System.out.println("Kierra Price and Liz Kroemer\nLab 6\n" + new Date() + "\n---------------\n");
	
		// DNA String Imput
		System.out.println("Please enter a DNA String with only C, G, T, and A: ");
		dnaString = scan.next();
		dnaString = dnaString.toUpperCase();
		System.out.println("Your DNA String is: " + dnaString);

		// Compliment of DNA String
		dnaString = dnaString.toLowerCase();
		
		s2 = dnaString.replace('a','T'); // These change the placeholding lowercase letters into uppercase letters
		s2 = s2.replace('t','A');
		s2 = s2.replace('g','C');
		s2 = s2.replace('c','G');
		

		System.out.println("Your compliment DNA is: " + s2); // Print out of the compliment

		// Random DNA letter inserted into random location
		len = dnaString.length();		// length of dna String
		location = r.nextInt(len+1);		// Chooses random location
		dnaString = dnaString.toUpperCase();
	
		s3 = dnaString.substring(0,location);		// the substring of the letters leading up to the location
		s4 = dnaString.substring(location);		// remaining letters after the location
		
			
		System.out.println("Inserting " + a + " at position " + location + " gives " + s3 + a + s4);
	
		// Random mutation of removing a letter from a randomly chosen position 
		location2 = r.nextInt(len+1);
		s5 = dnaString.substring(0,location2);
		s6 = dnaString.substring(location2+1);


		System.out.println("Removing the letter from location " + location2 + " gives " + s5 + s6);

		// Random mutation of altering a single letter from a random position and changed to a randomly chosen letter from ACTG
		location3 = r.nextInt(len+1);
		s7 = dnaString.substring(0,location3);
		s8 = dnaString.substring(location3+1);


		System.out.println("Changing position " + location3 + " with " + b + " gives " + s7 + b + s8);








	}
}
